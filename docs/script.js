// values
class Ability{
  constructor(){}
}

class Combine extends Ability{
  constructor({types}){
    super();
    this.types = types;
    this.type = 'combine';
  }
}

class Swap extends Ability{
  constructor({types}){
    super();
    this.types = types;
    this.type = 'swap';
  }
}

class Card{
  constructor({
    abilities = [],
    defeats = [],
    inPlay = false,
    type
  }){
    this.abilities = abilities;
    this.defeats = defeats;
    this.inPlay = inPlay;
    this.type = type;
  }
}

class Red extends Card {
  constructor(){
    super({
      defeats: ['green'],
      abilities: [new Combine({types: ['red']})],
      type: 'red'
    });
  }
}

class Green extends Card {
  constructor(){
    super({
      defeats: ['blue'],
      abilities: [new Swap({types: ['red', 'blue', 'purple']})],
      type: 'green'
    });
  }
}

class Blue extends Card {
  constructor(){
    super({
      defeats: ['red'],
      abilities: [new Swap({types: ['purple']})],
      type: 'blue'
    });
  }
}

class Purple extends Card {
  constructor(){
    super({
      defeats: ['red', 'blue', 'green'],
      abilities: [],
      type: 'purple'
    });
  }
}

const cardTypes = {
  'red': Red,
  'green': Green,
  'blue': Blue,
  'purple': Purple
};

const numberOfCards = 3;

class Player{
  constructor({
    hand = [],
    score = 0,
    pass = false
  } = {}){
    this.hand = hand.map(card => new cardTypes[card.type]);
    this.score = score;
    this.pass = pass;
  }
}

const computerDelay = 1000;

class Computer extends Player{
  constructor({
    hand,
    score,
    difficulty = 'easy'
  } = {}){
    super({
      hand,
      score
    });
    this.difficulty = difficulty;
  }
}

class Game{
  constructor({
    mode = 'build',
    turn = 'player',
    winner
  } = {}){
    // build
    // play
    // over
    this.mode = mode;
    this.turn = turn;
    this.winner = winner;
  }
}

// elements
function cardInfoElement(type){
  const card = new cardTypes[type]();
  const div = document.createElement('div');
  div.classList.add('card-info');

  const defeatsLabel = document.createElement('div');
  defeatsLabel.textContent = 'defeats';
  div.appendChild(defeatsLabel);

  const defeats = document.createElement('div');
  defeats.classList.add('info-row');
  card.defeats.forEach(d => defeats.appendChild(iconElement(`icon-${DRAGON_TYPES[d]}`)));
  div.appendChild(defeats);

  if(card.abilities[0]){
    const abilitiesLabel = document.createElement('div');
    abilitiesLabel.textContent = card.abilities[0].type + 's with';
    div.appendChild(abilitiesLabel);

    const abilities = document.createElement('div');
    abilities.classList.add('info-row');
    card.abilities[0].types.forEach(d => abilities.appendChild(iconElement(`icon-${DRAGON_TYPES[d]}`)));
    div.appendChild(abilities);
  }

  return div;
}

function playMenuIconElement(){
  const div = document.createElement('div');
  div.classList.add('hamburger');
  div.textContent = '|||';
  return div;
}

function helpIconElement(){
  const div = document.createElement('div');
  div.classList.add('help');
  div.textContent = '?';
  return div;
}

function optionsElement(){
  const div = document.createElement('div');
  div.classList.add('options');
  div.appendChild(playMenuIconElement());
  div.appendChild(helpIconElement());
  return div;
}

function scoreElement(){
  const div = document.createElement('div');
  div.classList.add('score');
  const c = document.createElement('div');
  c.classList.add('computer-score', 'score-item');
  div.appendChild(c);
  const p = document.createElement('div');
  p.classList.add('player-score', 'score-item');
  div.appendChild(p);
  return div;
}

function handElement(type){
  const div = document.createElement('div');
  div.classList.add('hand', `hand-${type}`);
  return div;
}

function playerCardsElement(cards, type){
  const div = document.createElement('div');
  div.classList.add('cards', `cards-${type}`);
  cards.forEach(card => div.appendChild(cardElement(card.type, card)));
  return div;
}

function iconElement(name){
  const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svg.classList.add('icon', `${name}`);
  const use = document.createElementNS('http://www.w3.org/2000/svg', 'use');
  use.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', `#${name}`);
  svg.appendChild(use);
  return svg;
}

function dragonElement(name){
  const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svg.classList.add('dragon', `${name}`);
  const use = document.createElementNS('http://www.w3.org/2000/svg', 'use');
  use.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', `#${name}`);
  svg.appendChild(use);
  return svg;
}

const DRAGON_TYPES = {
  red:'fire',
  blue: 'water',
  green: 'forest',
  purple: 'lotus'
};

function cardElement(type, card){
  const div = document.createElement('div');
  div.classList.add('card', `card-${type}`);
  div.setAttribute('data-card-type', type);
  div.appendChild(dragonElement(`dragon-${type}`));
  if(card){
    div._card = card;
  }
  div.appendChild(iconElement('icon-logo'));
  div.appendChild(iconElement(`icon-${DRAGON_TYPES[type]}`));
  div.appendChild(cardInfoElement(type));
  return div;
}

function cardSetElement(type, number){
  const div = document.createElement('div');
  div.classList.add('card-set');
  while(number--){
    div.appendChild(cardElement(type));
  }
  return div;
}

function tableElement(){
  const div = document.createElement('div');
  div.classList.add('table');
  return div;
}

function menuButton(){
  const button = document.createElement('button');
  button.appendChild(iconElement('menu'));
  return button;
}

function menuItemElement(item){
  const div = document.createElement('div');
  div.classList.add('menu-item');
  div.textContent = item.text;
  return div;
}

const MENU_ITEMS = [
  {view: 'play', text: 'Play'},
  // {view: 'settings', text: 'Settings'}
];

function menuViewElement(){
  const div = document.createElement('div');
  div.classList.add('menu');
  const h1 = document.createElement('h1');
  h1.textContent = document.title; // lol got em
  div.appendChild(iconElement('icon-logo'));
  div.appendChild(h1);
  MENU_ITEMS.forEach(item => {
    div.appendChild(viewUpdateElement(menuItemElement(item), item.view));
  });
  return viewElement(div, '');
}

function playViewElement(){
  const div = document.createElement('div');
  div.classList.add('play');
  div.appendChild(scoreElement());
  div.appendChild(optionsElement());
  div.appendChild(handElement('computer'));
  div.appendChild(tableElement());
  div.appendChild(handElement('player'));
  return viewElement(div, 'play');
}

const PLAY_MENU_ITEMS = [
  {text: 'Resume', action: 'togglePlayMenuAction'},
  {text: 'New Game', action: 'newGameAction'},
  {text: 'Main Menu', action: 'mainMenuAction'}
];

function playMenuElement(){
  const div = document.createElement('div');
  div.classList.add('play-menu');
  PLAY_MENU_ITEMS.forEach(item => {
    div.appendChild(playMenuItem(menuItemElement(item), item.action));
  });
  return div;
}

function playMenuItem(node, action){
  node.setAttribute('data-action', action);
  return node;
}

function viewElement(node, view){
  node.setAttribute('data-view', view);
  return node;
}

function viewUpdateElement(node, view){
  node.setAttribute('data-view-update', view);
  return node;
}

// actions
function togglePlayMenuAction(state, value){
  if(typeof value === 'boolean'){
    state.playMenu = false;
  }else{
    state.playMenu = !state.playMenu;
  }
}

function newGameAction(state, actions){
  actions.updateGameAction(state, new Game());
  actions.updatePlayerAction(state, new Player());
  actions.updateComputerAction(state, new Computer());
  actions.togglePlayMenuAction(state, false);
}

function mainMenuAction(state, actions){
  actions.togglePlayMenuAction(state, false);
  actions.updateViewAction(state, '');
}

function updateViewAction(state, view){
  state.view = view;
}

function updateGameAction(state, value){
  state.game = value;
}

function updateComputerAction(state, value){
  state.computer = value;
}

function updatePlayerAction(state, value){
  state.player = value;
}

function toggleRulesAction(state){
  state.showRules = !state.showRules;
}

// bindings
function optionsBinding({state, actions, elements}){
  const hamburger = elements.querySelector('.hamburger');
  const help = elements.querySelector('.help');
  const rules = document.querySelector('.rules');
  const rulesBack = document.querySelector('[data-action=toggleRulesAction]');
  hamburger.addEventListener('click', ()=>{
    actions.togglePlayMenuAction(state);
  });
  rulesBack.addEventListener('click', ()=>{
    actions.toggleRulesAction(state);
  });
  actions.toggleRulesAction(()=>{
    rules.classList.toggle('show', state.showRules);
  });
  help.addEventListener('click', ()=>{
    actions.toggleRulesAction(state);
  });
}

function resumeBinding({state, actions, elements}){
  const elms = Array.prototype.slice.call(elements.querySelectorAll('[data-action=togglePlayMenuAction]'));
  if(elms.length){
    actions.updateGameAction(()=>{
      elms.forEach(elm => {
        elm.classList.toggle('show', state.game.mode !== 'over');
      });
    });
  }
}

function triggerActionBinding({state, actions, elements}){
  const elms = Array.prototype.slice.call(elements.querySelectorAll('[data-action]'));
  if(elms.length){
    elms.forEach(elm => {
      elm.addEventListener('click', ()=>{
        actions[elm.getAttribute('data-action')](state, actions);
      });
    });
  }
}

function gameBinding({state, actions, elements}){
  // this binding will track the game, and player states
  let lastTurn = null;
  let playerHandCount = state.player.hand.length;
  let computerHandCount = state.computer.hand.length;
  let lastPlayerPass = state.player.pass;
  let lastComputerPass = state.computer.pass;

  actions.updateGameAction(()=>{
    if(state.game.mode === 'build'){
      if(state.game.turn !== lastTurn && isComputersTurn(state)){
        const cards = remainingCards(state);
        setTimeout(()=>{
          computerSelectCard(state, actions, cards);
        }, computerDelay);
      }
    }else if(state.game.mode === 'play'){
      if(isComputersTurn(state)){
        setTimeout(()=>{
          computerPlayCard(state, actions);
        }, computerDelay);
      }
    }else if(state.game.mode === 'over'){
      // do anything?
    }
    lastTurn = state.game.turn;
  });

  actions.updatePlayerAction(()=>{
    if(state.game.mode === 'build'){
      if(state.player.hand.length !== playerHandCount){
        if(state.player.hand.length === 6 && state.computer.hand.length === 6){
          lastTurn = null;
          state.game.mode = 'play';
          state.game.turn = 'computer'; //<-fix this
          actions.updateGameAction(state, state.game);
        }else{
          state.game.turn = 'computer';
          actions.updateGameAction(state, state.game);
        }
      }
    }else if(state.game.mode === 'play'){
      if(state.player.pass && state.computer.pass){
        scoreRound(state, actions);
        if(!playerHasCardsToPlay(state, 'player') || !playerHasCardsToPlay(state, 'computer')){
          state.game.mode = 'over';
          actions.updateGameAction(state, state.game);
        }
      }else if(state.player.pass === true && lastPlayerPass === false){
        state.game.turn = 'computer';
        actions.updateGameAction(state, state.game);
      }
    }else if(state.game.mode === 'over'){
      // do anything?
    }
    playerHandCount = state.player.hand.length;
    lastPlayerPass = state.player.pass;
  });

  actions.updateComputerAction(()=>{
    if(state.game.mode === 'build'){
      if(state.computer.hand.length !== computerHandCount){
        if(state.player.hand.length === 6 && state.computer.hand.length === 6){
          lastTurn = null;
          state.game.mode = 'play';
          state.game.turn = 'computer'; //<-fix this
          actions.updateGameAction(state, state.game);
        }else{
          state.game.turn = 'player';
          actions.updateGameAction(state, state.game);
        }
      }
    }else if(state.game.mode === 'play'){
      if(state.player.pass && state.computer.pass){
        scoreRound(state, actions);
        if(!playerHasCardsToPlay(state, 'player') || !playerHasCardsToPlay(state, 'computer')){
          state.game.mode = 'over';
          actions.updateGameAction(state, state.game);
        }
      }else if(state.computer.pass === true && lastComputerPass === false){
        state.game.turn = 'player';
        actions.updateGameAction(state, state.game);
      }
    }else if(state.game.mode === 'over'){
      // do anything?
    }
    computerHandCount = state.computer.hand.length;
    lastComputerPass = state.computer.pass;
  });
}

function playerHandBinding({state, actions, elements}){
  const elms = Array.prototype.slice.call(elements.querySelectorAll('.hand-player'));
  if(elms.length){
    actions.updatePlayerAction((s, v)=>{
      elms.forEach(elm => {
        elm.innerHTML = '';
        const cardsElm = playerCardsElement(state.player.hand.filter(c => !c.inPlay), 'player');
        bindState({
          state,
          bindings: [
            cardBinding
          ],
          actions,
          elements: cardsElm
        });
        elm.appendChild(cardsElm);
      });
    });
  }
}

function computerHandBinding({state, actions, elements}){
  const elms = Array.prototype.slice.call(elements.querySelectorAll('.hand-computer'));
  if(elms.length){
    actions.updateComputerAction((s, v)=>{
      elms.forEach(elm => {
        elm.innerHTML = '';
        const cardsElm = playerCardsElement(state.computer.hand.filter(c => !c.inPlay), 'computer');
        elm.appendChild(cardsElm);
      });
    });
  }
}

function storeStateBinding({state, actions}){
  Object.keys(actions).forEach(n => actions[n](()=>{setState(state)}));
}

function playMenuBinding({elements, actions, state}){
  const elms = Array.prototype.slice.call(elements.querySelectorAll('.play-menu'));
  if(elms.length){
    function onUpdate(){
      elms.forEach(elm => elm.classList.toggle('show', state.playMenu || state.game.mode === 'over' && state.view === 'play'));
    }
    actions.togglePlayMenuAction(onUpdate);
    actions.updateGameAction(onUpdate);
    actions.updateViewAction(onUpdate);
  }
}

function scoreBinding({state, actions, elements}){
  const scoreElm = elements.querySelector('.score');
  const compScore = elements.querySelector('.computer-score');
  const playerScore = elements.querySelector('.player-score');
  function onUpdate(){
    scoreElm.classList.toggle('show', state.game.mode !== 'build');
    compScore.setAttribute('data-value', formatScoreToRoman(state.computer.score));
    playerScore.setAttribute('data-value', formatScoreToRoman(state.player.score));
    compScore.classList.toggle('score-item-active', state.game.turn === 'computer');
    playerScore.classList.toggle('score-item-active', state.game.turn === 'player');
    compScore.classList.toggle('score-item-pass', state.computer.pass);
    playerScore.classList.toggle('score-item-pass', state.player.pass);
  }
  actions.updatePlayerAction(onUpdate);
  actions.updateComputerAction(onUpdate);
  actions.updateGameAction(onUpdate);
  playerScore.addEventListener('click', ()=>{
    if(playerCanPass(state)){
      state.player.pass = true;
      actions.updatePlayerAction(state, state.player);
    }
  });
}

function viewBinding({
  actions,
  bindings,
  elements,
  state
 }){
  window.addEventListener('pageshow', event => {
    console.log('pageshow');
    actions.updateViewAction(state, ''); // kick it off
  });
}

function viewUpdateBinding({actions, elements, state}){
  const elms = Array.prototype.slice.call(elements.querySelectorAll('[data-view-update]'));
  if(elms.length){
    elms.forEach(elm => elm.addEventListener('click', (evt)=>{
      evt.preventDefault();
      actions.updateViewAction(state, elm.getAttribute('data-view-update'));
    }));
  }
}

function viewElementBinding({elements, actions}){
  const elms = Array.prototype.slice.call(elements.querySelectorAll('[data-view]'));
  if(elms.length){
    actions.updateViewAction((state, view)=>{
      elms.forEach(elm => elm.classList.toggle('show', elm.getAttribute('data-view') === view));
    });
  }
}

function tableElementBinding({elements, actions, state, bindings}){
  const elms = Array.prototype.slice.call(elements.querySelectorAll('.table'));
  if(elms.length){
    const playerHandUpdate = () => {
      elms.forEach((elm)=>{
        elm.innerHTML = '';
        if(state.game.mode === 'build'){
          // show build table -> get the number of each card not in a players hand
          const toHide = {};
          // getting here before having player or computer
          state.player.hand.forEach(card => {
            if(!toHide[card.type]){
              toHide[card.type] = 0;
            }
            toHide[card.type] = toHide[card.type] + 1;
          });
          state.computer.hand.forEach(card => {
            if(!toHide[card.type]){
              toHide[card.type] = 0;
            }
            toHide[card.type] = toHide[card.type] + 1;
          });
          Object.keys(cardTypes).forEach(type => {
            const cardSet = cardSetElement(type, numberOfCards - (toHide[type] ? toHide[type] : 0));
            // need to inherit bindings
            bindState({
              actions,
              state,
              bindings: [
                cardBinding
              ],
              elements: cardSet
            });
            elm.appendChild(cardSet);
          });
        }else{
          // play or over, show game table
          // only append 2 cardSets as they will be rows
          const computerInPlay = state.computer.hand.filter(c => !!c.inPlay);
          const playerInPlay = state.player.hand.filter(c => !!c.inPlay);
          elm.appendChild(cardSetElement(computerInPlay[0] ? computerInPlay[0].type : null, computerInPlay.length));
          const p = cardSetElement(playerInPlay[0] ? playerInPlay[0].type : null, playerInPlay.length);
          elm.appendChild(p);
        }
      });
    };
    actions.updateGameAction(()=>{
      elms.forEach((elm)=>{
        elm.classList.toggle('table-build', state.game.mode === 'build');
      });
    });
    // anytime the player or computer update, based on the game mode, render the in play cards
    actions.updatePlayerAction(playerHandUpdate);
    actions.updateComputerAction(playerHandUpdate);
  }
}

let isBound = false;
let elms = []
function bindOnce({elements, actions, state}){
  elms = elms.concat(elements);
  if(!isBound){
    isBound = true;
    let active;
    let placeholder;
    let dropTarget;
    let currentX;
    let currentY;
    let initialX;
    let initialY;
    let xOffset = 0;
    let yOffset = 0;
    let startTime;
    let raf;

    document.addEventListener('touchstart', dragStart, {passive: false});
    document.addEventListener('touchend', dragEnd, {passive: false});
    document.addEventListener('touchmove', drag, {passive: false});

    document.addEventListener('mousedown', dragStart, {passive: false});
    document.addEventListener('mouseup', dragEnd, {passive: false});
    document.addEventListener('mousemove', drag, {passive: false});

    function dragStart(e){
      const elm = elms.find(elm => elm === e.target);
      let clientX;
      let clientY;
      startTime = new Date();
      if(elm){
        if(e.type === 'touchstart'){
          clientX = e.touches[0].clientX;
          clientY = e.touches[0].clientY;
          e.preventDefault();
        }else{
          clientX = e.clientX;
          clientY = e.clientY;
        }
        initialX = clientX - e.target.getBoundingClientRect().left;
        initialY = clientY - e.target.getBoundingClientRect().top;
        if(placeholder && active){
          removePlaceholder(placeholder, active);
          active = null;
          placeholder = null;
        }
        setTranslate(clientX - initialX, clientY - initialY, elm);
        active = elm;
        placeholder = setPlaceholder(active);
      }
    }

    function dragEnd(e){
      initialX = currentX;
      initialY = currentY;
      // check dropTarget, could be hand during build or table during play
      console.log('end')
      if(new Date() - startTime <= 200){
        let detail = active || document.querySelector('.card-detail');
        if(detail){
          detail.classList.toggle('card-detail');
          playSound('flip');
          if(detail.classList.contains('card-detail')){
            document.querySelector('.hand-player').classList.add('hand-fade');
            detail.classList.remove('card-active');
            detail.removeAttribute('style');
            return;
          }else{
            document.querySelector('.hand-player').classList.remove('hand-fade');
          }
        }

      }else if(isPlayersTurn(state) && active){
        const dropOnHand = dropTarget && dropTarget.closest('.hand-player');
        const dropOnTable = dropTarget && (dropTarget.closest('.table') || dropTarget.closest('.score'));
        if(dropOnHand && !active._card){
          const card = new cardTypes[active.getAttribute('data-card-type')]();
          state.player.hand.push(card);
          actions.updatePlayerAction(state, state.player);
        }
        if(dropOnTable && active._card && playerCanPlayCard(state, active._card)){
          playCard(state, actions, active._card, 'player');
        }
      }
      if(placeholder && active){
        removePlaceholder(placeholder, active);
      }
      dropTarget = null;
      active = null;
      placeholder = null;
    }

    function drag(e){
      if(active && placeholder){
        e.preventDefault();
        let clientX;
        let clientY;
        if(e.type === 'touchmove'){
          clientX = e.touches[0].clientX;
          clientY = e.touches[0].clientY;
        }else{
          clientX = e.clientX;
          clientY = e.clientY;
        }
        currentX = clientX - initialX;
        currentY = clientY - initialY;

        active.hidden = true;
        dropTarget = document.elementFromPoint(clientX, clientY);
        active.hidden = false;

        xOffset = currentX;
        yOffset = currentY;
        setTranslate(currentX, currentY, active);
      }
    }

    function setTranslate(x, y, elm){
      raf = requestAnimationFrame(()=>{
        elm.style.transform = `translate3d(${x}px, ${y}px, 0) scale(1.04)`;
        raf = null;
      });
    }

    function setPlaceholder(elm){
      let place = elm.cloneNode();
      place.setAttribute('style', 'visibility:hidden;');
      elm.parentNode.insertBefore(place, elm);
      elm.style.position = 'absolute';
      elm.classList.add('card-active');
      elm.style.zIndex = 1000;
      elm.style.top = 0;
      elm.style.left = 0;
      document.body.append(elm);
      return place;
    }

    function removePlaceholder(placeholder, elm, insert = true){
      if(raf){
        cancelAnimationFrame(raf);
        raf = null;
      }
      elm.classList.remove('card-active');
      elm.removeAttribute('style');
      if(insert){
        placeholder.parentNode.insertBefore(elm, placeholder);
      }else{
        elm.remove();
      }
      placeholder.remove();
    }
  }
}

// player should only be able to drag unassigned cards not in anyone's hand or
// cards in their hand
function cardBinding({elements, actions, state}){
  const elms = Array.prototype.slice.call(elements.querySelectorAll('.card'));
  if(elms.length){
    bindOnce({elements: elms, actions, state});

  }
}

// format
const romanNumerals = ['','I','II','III','IV', 'V', 'VI'];
function formatScoreToRoman(score){
  return romanNumerals[score];
}

// policies
function playerHasCardInPlay(state, player = 'player'){
  return state[player].hand.some(c => c.inPlay);
}

function isPlayersTurn(state, player = 'player'){
  return state.game && state.game.turn === player;
}

function playerCanPass(state, player = 'player'){
  return isPlayersTurn(state, player) && playerHasCardInPlay(state, player) && state[player].pass === false;
}

function isComputersTurn(state){
  return isPlayersTurn(state, 'computer');
}

function playerHasCardsToPlay(state, player = 'player'){
  return state[player].hand.some(c => !c.inPlay);
}

function playerCanPlayCard(state, card){
  const cards = state.player.hand.filter(c => c.inPlay);
  if(state.game.mode !== 'play'){
    return false;
  }
  if(cards.length > 1){
    return false;
  }
  return !cards.length || cards[0].abilities.some(a => a.types.some(t => card.type === t));
}

function playerCanUseInPlayAbility(state, player = 'player'){
  const cards = state[player].hand.filter(c => c.inPlay);
  if(cards.length > 1){
    return false;
  }
  const typesNotInPlay = state[player].hand.filter(c => !c.inPlay).map(c => c.type);
  return cards[0].abilities.some(a => a.types.some(t => typesNotInPlay.indexOf(t) !== -1));
}

// services
function getInPlayCards(state, player = 'player'){
  return state[player].hand.filter(c => c.inPlay);
}

function playCard(state, actions, card, player = 'player'){
  const inPlay = getInPlayCards(state, player);
  // doesn't check multiple abilities
  if(inPlay.length && inPlay[0].abilities[0].type === 'swap'){
    inPlay[0].inPlay = false;
  }
  card.inPlay = true;
  state.player.pass = false;
  state.computer.pass = false;
  state.game.turn = player === 'player' ? 'computer' : 'player';
  actions.updateGameAction(state, state.game);
  actions.updateComputerAction(state, state.computer);
  actions.updatePlayerAction(state, state.player);
  playSound(soundType[card.type]);
}

function removeInplay(state, player = 'player'){
  let inPlay = state[player].hand.find(c => c.inPlay);
  while(inPlay){
    state[player].hand.splice(state[player].hand.indexOf(inPlay), 1);
    inPlay = state[player].hand.find(c => c.inPlay);
  }
}

function scoreRound(state, actions){
  const winner = roundWinner(state);
  removeInplay(state);
  removeInplay(state, 'computer');
  if(winner === 'draw'){
    // state.game.turn = state.game.lastWinner;
  }else if(winner === 'player'){
    state.player.score = state.player.score + 1;
    // state.game.turn = winner;
    // state.game.lastWinner = winner;
  }else{
    state.computer.score = state.computer.score + 1;
    // state.game.turn = winner;
    // state.game.lastWinner = winner;
  }
  state.player.pass = false;
  state.computer.pass = false;
  actions.updatePlayerAction(state, state.player);
  actions.updateComputerAction(state, state.computer);

  if(winner === 'draw'){
    state.game.turn = state.game.winner || state.game.turn === 'player' ? 'computer' : 'player';
  }else if(winner === 'player'){
    state.game.turn = winner;
    state.game.winner = winner;
  }else{
    state.game.turn = winner;
    state.game.winner = winner;
  }
  actions.updateGameAction(state, state.game);
}

function findAbilityCard(state, player = 'player'){
  const cardsNotInPlay = state[player].hand.filter(c => !c.inPlay);
  const cards = state[player].hand.filter(c => c.inPlay);
  let card;
  let ability;
  state[player].hand.some(c => {
     cards[0].abilities.some(a => {
      a.types.some(t => {
        card = cardsNotInPlay.find(ca => ca.type === t);
        if(card){
          ability = a;
          return true;
        }
      });
    });
  });
  return {
    ability,
    card
  };
}

function roundWinner(state){
  const playerCardsInPlay = state.player.hand.filter(c => !!c.inPlay);
  const computerCardsInPlay = state.computer.hand.filter(c => !!c.inPlay);
  if(playerCardsInPlay[0].defeats.indexOf(computerCardsInPlay[0].type) !== -1){
    return playerCardsInPlay.length >= computerCardsInPlay.length ? 'player' : 'computer';
  }
  if(computerCardsInPlay[0].defeats.indexOf(playerCardsInPlay[0].type) !== -1){
    return computerCardsInPlay.length >= playerCardsInPlay.length ? 'computer' : 'player';
  }
  if(playerCardsInPlay.length > computerCardsInPlay.length){
    return 'player';
  }
  if(playerCardsInPlay.length < computerCardsInPlay.length){
    return 'computer';
  }
  return 'draw';
}

const computerPlayOrder = [
 'green',
 'blue',
 'red',
 'purple'
];

function computerPlayCard(state, actions){
  const playerInPlay = state.player.hand.filter(c => !!c.inPlay);
  const compInPlay = state.computer.hand.filter(c => !!c.inPlay);
  if(playerInPlay.length && compInPlay.length){
    if(roundWinner(state) === 'computer' || !playerCanUseInPlayAbility(state, 'computer')){
      state.computer.pass = true;
      actions.updateComputerAction(state, state.computer);
    }else{
      const {card, ability} = findAbilityCard(state, 'computer');
      playCard(state, actions, card, 'computer');
      // if(ability.type === 'swap'){
      //   compInPlay[0].inPlay = false;
      //   card.inPlay = true;
      //   state.computer.pass = false;
      //   state.player.pass = false;
      //   actions.updateComputerAction(state, state.computer);
      // }else if(ability.type === 'combine'){
      //   card.inPlay = true;
      //   state.computer.pass = false;
      //   state.player.pass = false;
      //   actions.updateComputerAction(state, state.computer);
      // }
    }
  }else{
    let toPlay;
    computerPlayOrder.some(type => {
      toPlay = state.computer.hand.find(c => c.type === type);
      if(toPlay){
        return true;
      }
    });
    if(toPlay){
      playCard(state, actions, toPlay, 'computer');
      // toPlay.inPlay = true;
      // state.computer.pass = false;
      // state.player.pass = false;
      // actions.updateComputerAction(state, state.computer);
    }else{
      // do anything?
      state.computer.pass = true;
      actions.updateComputerAction(state, state.computer);
    }
  }
}

function remainingCards(state){
  const cards = [];
  Object.keys(cardTypes).forEach(type => {
    const count = state.player.hand.filter(c => c.type === type).length +
      state.computer.hand.filter(c => c.type === type).length;
    let l = numberOfCards - count;
    while(l--){
      cards.push(type);
    }
  });
  return cards;
}

function computerSelectCard(state, actions, cards){
  // based on computer difficulty
  if(cards.find(c => c === 'purple')){
    selectCard(state, actions, 'computer', 'purple');
  }else if(cards.find(c => c === 'red')){
    selectCard(state, actions, 'computer', 'red');
  }else if(cards.find(c => c === 'green')){
    selectCard(state, actions, 'computer', 'green');
  }else if(cards.find(c => c === 'blue')){
    selectCard(state, actions, 'computer', 'blue');
  }
}

function selectCard(state, actions, player, type){
  const card = new cardTypes[type]();
  state[player].hand.push(card);
  if(player === 'player'){
    actions.updatePlayerAction(state, state[player]);
  }else{
    actions.updateComputerAction(state, state[player]);
  }
}

const soundType = {
  red: 'fire',
  green: 'forest',
  blue: 'water',
  purple: 'growl'
};

const sounds = {
  fire: [0, 3],
  forest: [4, 7],
  water: [8, 11],
  growl: [12, 15],
  flip: [16, 17],
};

function playSound(sound){
  const s = document.getElementById(`${sound}-sfx`);
  s.currentTime = sounds[sound][0];
  const stopTime = sounds[sound][1];
  s.addEventListener('timeupdate', ()=>{
    if(s.currentTime >= stopTime){
      s.pause();
    }
  }, false);
  s.play();
}

function getState(state = {}){
  return Object.assign(
    {},
    state,
    JSON.parse(localStorage.getItem('state') || '{}')
  );
}

function setState(state){
  localStorage.setItem('state', JSON.stringify(state));
}

function bindState(config){
  config.bindings.forEach(b => b(config));
}

function mountElements(dom){
  const p = document.createDocumentFragment();
  dom.forEach(d => p.appendChild(d()));
  return p;
}

function createAction(action){
  const listeners = [];
  return function(...args){
    if(args[0].call){
      listeners.push(args[0]);
      return;
    }
    action(...args);
    listeners.forEach(l => l(...args));
  }
}

function createActions(actions){
  const a = {};
  Object.keys(actions).forEach(key => a[key] = createAction(actions[key]));
  return a;
}

function init({
  actions,
  bindings,
  elements,
  state
}){
  // construct from serialized data
  state.player = new Player(state.player);
  state.computer = new Computer(state.computer);
  state.game = new Game(state.game);

  // mount and bind
  actions = createActions(actions);
  elements = mountElements(elements);
  bindState({
    actions,
    bindings,
    elements,
    state
  });

  // distpatch actions
  actions.updatePlayerAction(state, state.player);
  actions.updateComputerAction(state, state.computer);
  actions.updateGameAction(state, state.game);

  // lil haxx
  state.playMenu = true;
  actions.togglePlayMenuAction(state);
  state.showRules = true;
  actions.toggleRulesAction(state);

  document.body.appendChild(elements);
}

init({
  actions: {
    updateViewAction,
    togglePlayMenuAction,
    updateGameAction,
    updateComputerAction,
    updatePlayerAction,
    togglePlayMenuAction,
    newGameAction,
    mainMenuAction,
    toggleRulesAction
  },
  bindings: [
    storeStateBinding,
    viewBinding,
    viewElementBinding,
    viewUpdateBinding,
    playMenuBinding,
    tableElementBinding,
    playerHandBinding,
    computerHandBinding,
    gameBinding,
    scoreBinding,
    triggerActionBinding,
    resumeBinding,
    optionsBinding
  ],
  elements: [
    menuViewElement,
    playViewElement,
    playMenuElement
  ],
  state: getState()
});
