# Dragon Card Game

## Rules
When the game starts the players take turns selecting one card at a time to include in their hand.
When all the cards have been selected the winner from the last game starts the first round.
If there was no winner or it's the first game, a random player is chosen to start.
During a round players alternate playing cards from their hand until both players pass in succession.
Each player must play at least one card each round.
If there is a winner of the round they score a victory point and start next round.
If there was no winner, the previous player to start the round starts the next round.
When a player runs out of cards the game is over.
The player with the most victory points wins.

## Victory
A victory is determined by the color and number of each card in a round.
The cards color defines which other color it can defeat and what ability it has if any.

## Abilities
Cards have abilities which allow the player to take more turns during a round.
During a players turn they may use the ability of the last played card.

- ### Combine
The combine ability allows the player to play another card of a particular color.

- ### Swap
The swap ability allows the player to replace the last played card with a card from their hand.

## Colors

- ### Red
 - Defeats: Green
 - Ability: can combine with Red

- ### Green
 - Defeats: Blue
 - Ability: can swap with Red, Blue, Purple

- ### Blue
 - Defeats: Red
 - Ability: can swap with Purple

- ### Purple
 - Defeats: Red, Green, and Blue
 - Ability: none
